import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import counterReducer from './slices/counter';
import authReducer from './slices/auth';
import { pokemonApi } from './services/pokemonApi';
import { authApi } from './services/authApi';
import { baseApi } from './services/baseApi';

const store = configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    [baseApi.reducerPath]: baseApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(pokemonApi.middleware)
      .concat(authApi.middleware)
      .concat(baseApi.middleware),
});

setupListeners(store.dispatch);

export default store;

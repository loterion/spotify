import React from 'react';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import { Box } from '@chakra-ui/react';
import Login from './components/Login';
import RequireToken from './utils/RequireToken';
import Dashboard from './components/Dashboard';
import NotFound from './components/NotFound';

function App() {
  return (
    <Box>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route element={<RequireToken />}>
          <Route path="/dashboard" element={<Dashboard />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Box>
  );
}

export default App;

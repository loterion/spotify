/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const apiSlice = createSlice({
  name: 'baseApi',
  initialState: {
    featured: {},
  },
  reducers: {
    setFeatured: (state, { payload }) => {
      state.featured = payload;
    },
  },
});

export const { setFeatured } = apiSlice.actions;
export default apiSlice.reducer;
export const selectFeatured = (state) => state.baseApi.featured;
export const selectApi = (state) => state.baseApi;

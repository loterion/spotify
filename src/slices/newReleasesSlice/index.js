import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const newReleasesSlice = createSlice({
    name: 'newReleases',
    initialState,
    reducers: {}
})

export default newReleasesSlice.reducer;
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Navigate, useLocation, Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectToken } from '../../slices/auth';

export default function RequireToken() {
  const token = useSelector(selectToken);
  const location = useLocation();

  if (!token) {
    return <Navigate to="/" state={{ from: location }} />;
  }

  return <Outlet />;
}

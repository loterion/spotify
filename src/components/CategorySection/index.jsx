import React from "react";
import {
  Box,
  Button,
  Flex,
  Heading,
  Image,
  Stack,
  Text,
} from "@chakra-ui/react";
import Slider from "react-slick";

function Content({ data }) {
  const responsive = [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ];

  return (
    <Slider
      dots
      infinite
      speed={500}
      slidesToShow={6}
      slidesToScroll={6}
      responsive={responsive}
    >
      {data.playlists.items.map((item) => (
        <Box key={item.id}>
          <Box
            borderWidth="1px"
            borderRadius="lg"
            m={2}
            bg="white"
            boxShadow="2xl"
          >
            <Flex flex={1} bg="blue.200">
              <Image src={item.images[0].url} />
            </Flex>
            <Stack
              flex={1}
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              p={2}
              h="200px"
            >
              <Heading fontSize="xl" fontFamily="body">
                {item.name}
              </Heading>
              <Text fontWeight={600} color="gray.500" size="sm" mb={4}>
                {`@${item.owner.display_name}`}
              </Text>
              <Text fontSize="xs" textAlign="center" color="gray.700" px={3}>
                <span
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{
                    __html:
                      item.description.length > 80
                        ? `${item.description.slice(0, 80)}...`
                        : item.description,
                  }}
                />
              </Text>
              <Stack
                width="100%"
                mt="2rem"
                direction="row"
                padding={2}
                justifyContent="space-between"
                alignItems="center"
              >
                <Button
                  flex={1}
                  fontSize="xs"
                  rounded="full"
                  _focus={{
                    bg: "gray.200",
                  }}
                >
                  URL
                </Button>
                <Button
                  flex={1}
                  fontSize="xs"
                  rounded="full"
                  bg="blue.400"
                  color="white"
                  boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
                  _hover={{
                    bg: "blue.500",
                  }}
                  _focus={{
                    bg: "blue.500",
                  }}
                >
                  Spotify URI
                </Button>
              </Stack>
            </Stack>
          </Box>
        </Box>
      ))}
    </Slider>
  );
}

export default function CategorySection({
  data,
  isLoading,
  isFetching,
  isSuccess,
}) {
  return (
    <Box mt={20} mb={20}>
      {isLoading || (isFetching && "Loading...")}
      {isSuccess && data && data.playlists ? (
        <Box>
          <Heading textAlign="center" mb={4}>
            {data.message}
          </Heading>
          <Content data={data} />
        </Box>
      ) : null}
    </Box>
  );
}

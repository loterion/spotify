import React from 'react';
import { Heading } from '@chakra-ui/react';
import { useFeaturedPlaylistsQuery, useGenresQuery } from '../../services/baseApi';
import CategorySection from '../CategorySection';
import Genres from '../Genres';

export default function Dashboard() {
  const {
    data: dataF,
    isLoading: isLoadingF,
    isFetching: isFetchingF,
    isSuccess: isSuccessF,
  } = useFeaturedPlaylistsQuery();
  const { data: dataG } = useGenresQuery();
  return (
    <>
      <Heading backgroundColor="black" color="white" fontSize="xl" textAlign="center" mb={4} p={4}>
        Spotify dashboard
      </Heading>
      <CategorySection
        data={dataF}
        isLoading={isLoadingF}
        isFetching={isFetchingF}
        isSuccess={isSuccessF}
      />
      <Genres data={dataG} />
    </>
  );
}

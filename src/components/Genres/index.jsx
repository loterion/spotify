import React from 'react';
import { Box, Text, Badge } from '@chakra-ui/react';

export default function Genres({ data }) {
  return (
    <Box p={4} mt={8}>
      <Text fontSize="xl" fontWeight="bold" textAlign="center">
        {data && data.genres.length
          ? data.genres.map((item) => (
              <Badge key={item} ml="1" fontSize="0.8em">
                {item.toUpperCase()}
              </Badge>
            ))
          : null}
      </Text>
    </Box>
  );
}

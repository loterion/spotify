/* eslint-disable prettier/prettier */
import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {increment, decrement} from '../../slices/counter';

export default function Counter() {
    const count = useSelector((state) => state.counter.value);
    const dispatch = useDispatch();

    return(
        <div>
            <button type="button" onClick={() => dispatch(increment())}>Increment</button>
            <button type="button" onClick={() => dispatch(decrement())}>Decrement</button>
            <span>{count}</span>
        </div>
    )
}
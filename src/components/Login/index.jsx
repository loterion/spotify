import React from 'react';
import { VStack, Button, Center, useToast } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setToken } from '../../slices/auth';
import { useGetTokenMutation } from '../../services/authApi';

export default function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const toast = useToast();

  const [getToken, { isLoading }] = useGetTokenMutation();

  const handleGetToken = async () => {
    try {
      const data = await getToken();
      dispatch(setToken(data.data.access_token));
      navigate('/dashboard', { replace: true });
    } catch (err) {
      toast({
        status: 'error',
        title: 'Error',
        description: 'Oh no! there was an error getting access token!',
        isClosable: true,
      });
    }
  };

  return (
    <Center h="500px">
      <VStack spacing="4">
        <Button isFullWidth onClick={handleGetToken} colorScheme="green" isLoading={isLoading}>
          Get token
        </Button>
      </VStack>
    </Center>
  );
}

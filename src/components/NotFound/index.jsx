import React from 'react';
import { Box } from '@chakra-ui/react';

export default function NotFound() {
  return <Box>Oops! Not found.</Box>;
}

import React from 'react';
import { Container, Stack, Text } from '@chakra-ui/react';

export default function Header({ children }) {
  return (
    <Container maxW="7xl">
      <Stack spacing={3}>
        <Text fontSize="6xl">{children}</Text>
      </Stack>
    </Container>
  );
}

import React from 'react';
import Counter from '../Counter';
import { useGetPokemonByNameQuery } from '../../services/pokemonApi';

function NewReleases() {
  const { data, error, isLoading } = useGetPokemonByNameQuery('bulbasaur');

  const content = () => {
    if (error) return 'Oh no, there is an error';

    if (isLoading) return 'Loading...';

    if (data) {
      return (
        <>
          <h3>{data.species.name}</h3>
          <img src={data.sprites.front_shiny} alt={data.species.name} />
        </>
      );
    }

    return null;
  };

  return (
    <>
      <h1>New Releases 🚀</h1>
      <div>{content}</div>
      <Counter />
    </>
  );
}

export default NewReleases;

import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const baseApi = createApi({
  reducerPath: 'baseApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://api.spotify.com/v1',
    prepareHeaders: (headers, { getState }) => {
      const { token } = getState().auth;
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    featuredPlaylists: builder.query({
      query: () => ({ url: '/browse/featured-playlists' }),
    }),
    genres: builder.query({
      query: () => ({ url: '/recommendations/available-genre-seeds' }),
    }),
    categories: builder.query({
      query: () => ({ url: '/browse/categories' }),
    }),
  }),
});

export const { useFeaturedPlaylistsQuery, useGenresQuery, useCategoriesQuery } = baseApi;

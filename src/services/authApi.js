import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://accounts.spotify.com',
    prepareHeaders: (headers) => {
      headers.set(
        'Authorization',
        // eslint-disable-next-line no-undef
        `Basic ${window.btoa(
          `${process.env.REACT_APP_CLIENT_ID}:${process.env.REACT_APP_CLIENT_SECRET}`
        )}`
      );
      // headers.set('Access-Control-Allow-Origin', '*');
      headers.set('content-type', 'application/x-www-form-urlencoded');
      return headers;
    },
  }),
  endpoints: (builder) => ({
    authorize: builder.query({
      query: () =>
        `authorize?client_id=303a8deb58864a118a559b2b500bfbe8&response_type=code&redirect_uri=http://localhost:3000`,
    }),
    getToken: builder.mutation({
      query: () => ({
        url: 'api/token',
        method: 'POST',
        body: 'grant_type=client_credentials',
      }),
    }),
    protected: builder.mutation({
      query: () => 'protected',
    }),
  }),
});

export const { useGetTokenMutation, useAuthorizeQuery, useProtectedMutation } = authApi;
